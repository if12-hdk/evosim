#include <SFML/Graphics.hpp>

using namespace std;

class Histogram {
    sf::VertexArray line;
    deque<double> data;
    unsigned int size = 64;
    double maximum = 0;
    sf::Color color;

public:
    Histogram(unsigned int size, sf::Color color);
    void setSize(unsigned int size);
    void addValue(const double &value);
    void setMaximum(double maximum);
    void draw(
        sf::RenderWindow &window,
        sf::Vector2f position,
        sf::Vector2f maxSize
    );
};

Histogram::Histogram(unsigned int size, sf::Color color) {
    line.setPrimitiveType(sf::LineStrip);
    this->size = size;
    this->color = color;
}

void Histogram::setSize(unsigned int size) {
    this->size = size;
}

void Histogram::addValue(const double &value) {
    this->data.push_back(value);
    if (data.size() > size) {
        data.pop_front();
    }
    maximum = max(maximum, value);
}

void Histogram::setMaximum(double maximum) {
    this->maximum = maximum;
}

void Histogram::draw(
    sf::RenderWindow &window,
    sf::Vector2f position,
    sf::Vector2f maxSize
) {
    sf::Text scaleInfo;
    sf::RectangleShape background;
    sf::Vertex next;

    unsigned int count = size;
    line.clear();

    for (int i = 0; i < count; i++) {
        if (i < data.size()) {
            next = sf::Vertex(
                position + (sf::Vector2f){
                    maxSize.x * i / (float)(count - 1),
                    -maxSize.y * data[i] / (float)maximum
                },
                color
            );
            //cout << next.position.x << " " << next.position.y << "\n";
            line.append(next);
        }
    }

    window.draw(line);
}