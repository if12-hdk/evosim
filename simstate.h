using namespace std;

struct SimulationState {
    int seed;
    const unsigned int worldWidth = WIDTH, worldHeight = HEIGHT;

    World world;

    sf::Vector2u worldSize {
        worldWidth,
        worldHeight
    };

    //
    CreatureList creatureList;

    SimulationState(int seed);
    void doFrame(unsigned int selectedID);
};

SimulationState::SimulationState(int seed) {
    this->seed = seed;
    world.init(seed);
    creatureList.init(seed);
    /*for (int i = creatureList.count(); i < minimumCreatureCount; i++) {
        creature.randomInit(
            nextID,
            BrainType::subleqBrain,
            randomEngine, randomXCoord, randomYCoord
        );
        nextID++;

        creatureList.addCreature(creature);
    }    */
}

void SimulationState::doFrame(unsigned int selectedID) {


    /*creature.cloneInit(nextID, {20.0, 20.0}, creature.getBrain(), randomEngine);
    nextID++;
    creatureList.addCreature(creature);*/

    creatureList.getValues(world);
    creatureList.applyUpdate(world, selectedID);

    world.update();
}
