#include <SFML/Graphics.hpp>
#include <cmath>
#include <random>
#include <deque>

//#define NO_IO

#define FRAMES_MEASURED_FOR_FPS 16

#define WIDTH 40
#define HEIGHT 40

//#define SEED 2996
#define WATER_LEVEL 0.46
//3477
#define MINIMUM_CREATURE_COUNT 60

#define STARTING_ENERGY 0.1
#define STARTING_SIZE 0.1

#define FIGHTING_ENERGY 0.001
#define FIGHTING_COST 0.0001
#define FIGHT_RANGE 2

#define STARTING_WALKING_SPEED 0.1
#define STARTING_SWIMMING_SPEED 0.06

#define MINIMUM_BIRTH_ENERGY 0.6
#define BIRTH_COST 0.5

#define MOVEMENT_IMPROVEMENT 0.0001
#define MOVEMENT_SPECIALIZATION 0.8

#define GRAZING_COST 0.0003
#define ENERGY_FROM_GROUND 0.02
#define METABOLISM 0.00002

#define MUTATION_CHANGES 0.004

#define WATER_MOVEMENT_COST 0.00005
#define LAND_MOVEMENT_COST 0.00001

#define DEAD_MATTER_GROWTH_RATIO 4

#define DEAD_MATTER_DECLINE 0.99995

#define UPDATES_PER_YEAR 4096
#define HISTOGRAM_UPDATE_FREQ 256

#define SMALL_TEXT_SIZE 12
#define MEDIUM_TEXT_SIZE 14
#define LARGE_TEXT_SIZE 18

#define LINE_SPACING 1.6

#include "util.h"

#include "world.h"
#include "creature_list.h"

#include "selected.h"

#include "simstate.h"
#include "renderstate.h"

using namespace std;

int main() {
    int seed = 420;
#ifndef NO_IO
    cout << "Please input an integer seed value: ";
    cin >> seed;
#endif

    SimulationState sim{seed};
    RenderState render {sim.worldSize};

    //cout << (int)TileType::land << " " << (int)TileType::water << "\n";

    //sim.doFrame({});

    // loop

    while (render.window.isOpen()) {

        // events

        sf::Event event;
        while (render.window.pollEvent(event)) {
            render.handleEvent(event, sim);
        }

        // simulation

        if (render.running) {
            for (int i = 0; i < render.updatesPerFrame; i++) {
                //cout << render.getSelectedCreature() << "\n";
                sim.doFrame(render.getSelectedCreature());
            }
        }

        // drawing

        render.drawInit();
        if (render.rendering) {
            render.drawWorld(sim.world, sim.creatureList);
        }
        render.drawInfo(sim.world, sim.creatureList);
        render.drawFinal();
    }

    return 0;
}
