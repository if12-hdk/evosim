#include <climits>

using namespace std;

struct RenderState {
    bool running = false;
    int updatesPerFrame = 1;

    bool rendering = true;
    bool scrolling = false;
    bool debug = true;

    sf::ContextSettings settings;

    sf::Time frameTime;
    sf::Time totalTime;
    deque<sf::Time> fpsMeter;
    sf::Clock clock;


    sf::View worldView, infoView;

    // text

    sf::Font font;

    sf::Text perfInfo, timeInfo, creatureInfo, updateInfo;

    vector<string> keyDocumentation;
    sf::Text keyInfo;


    vector<sf::RectangleShape> drawingTiles;
    int tileCount;


    Selected selected;

    DrawMode drawMode;

    sf::Vector2u screenSize {1248, 702};

    float worldPart = 0.5625f;
    float infoPart = 1.0f - worldPart;
    float scrollSensitivity = 1.1f;


    float displayScale;

    sf::Vector2f worldViewSize;

    sf::Vector2f viewLocation;
    sf::Vector2f viewCenter;

    sf::Vector2f infoViewSize;
    double infoScale = 1.0;

    sf::Vector2f
        originalMousePos, lastMousePos, newMousePos,
        pressLocation, totalScrolled;
    sf::Vector2f tileLocation;

    sf::RenderWindow window;

    sf::Texture screenshotTexture;
    sf::Image screenshot;



    RenderState(sf::Vector2u worldSize);

    void drawInit();
    void drawWorld(World &world, CreatureList &creatureList);
    void drawInfoHeader(World &world, CreatureList &creatureList);
    void drawInfo(World &world, CreatureList &creatureList);
    void drawFinal();

    unsigned int getSelectedCreature();
    sf::Vector2u getSelectedTile();

    void handleMouseRelease(
        sf::Event &event,
        SimulationState &sim
    );
    void handleKeyboardInput(
        sf::Event &event,
        SimulationState &sim
    );
    void handleEvent(
        sf::Event &event,
        SimulationState &sim
    );
};

RenderState::RenderState(sf::Vector2u worldSize) {
    settings.antialiasingLevel = 4;
    window.create(
            sf::VideoMode{screenSize.x, screenSize.y},
            "evosim",
            sf::Style::Default,
            settings
        );

    window.setFramerateLimit(60);

    if (!font.loadFromFile("Open_Sans/OpenSans-Regular.ttf")) {
#ifndef NO_IO
        cout << "!!! Open_Sans/OpenSans-Regular.ttf not found" << "\n";
#endif
    }

    textInit(perfInfo, font, MEDIUM_TEXT_SIZE, 10, 10);
    textInit(updateInfo, font, MEDIUM_TEXT_SIZE, 10,
        10 + lineSpacing(MEDIUM_TEXT_SIZE, infoScale)
    );
    textInit(creatureInfo, font, MEDIUM_TEXT_SIZE, 10, 10);
    textInit(timeInfo, font, MEDIUM_TEXT_SIZE, 10, 10);
    textInit(keyInfo, font, MEDIUM_TEXT_SIZE, 10, 70);

    keyDocumentation = {
        "Enter:\ttoggle simulation",
        "Tab:\trun simulation for one step",
        "+:\tincrease simulation speed",
        "-:\tdecrease simulation speed",
        "R:\ttoggle rendering",
        "Q:\ttoggle debug output",
        "Escape:\tclose selection",
        "F:\tfit world to screen",
        "C:\tcreate clone of selected creature",
        "B:\tsave screenshot",
        "O:\tset draw mode to original",
        "I:\tset draw mode to new",
        "E:\tset draw mode to showing elevation",
        "H:\tset draw mode to showing humidity",
        "T:\tset draw mode to showing temperature",
        "Y:\tset draw mode to showing energy",
        "A:\tcycle back in creature list",
        "D:\tcycle forward in creature list",
    };

    worldView.reset(sf::FloatRect(
        0, 0,
        (float)worldViewSize.x, (float)worldViewSize.y)
    );

    displayScale = fitWorldToScreen(worldSize, screenSize, worldPart);

    infoView.reset(sf::FloatRect(
        0, 0,
        (float)screenSize.x * infoPart, (float)screenSize.y)
    );

    worldViewSize = {
        ((float)screenSize.x) / displayScale * worldPart,
        ((float)screenSize.y) / displayScale
    };

    infoViewSize = {
        ((float)screenSize.x) * infoPart,
        ((float)screenSize.y)
    };

    //cout << infoViewSize.x << " " << infoViewSize.y << "\n";

    selected.type = SelectedType::Nothing;
    drawMode = DrawMode::drawOriginal;

    window.setActive(false);

    worldView.setViewport(sf::FloatRect(0.0f, 0.0f, worldPart, 1.0f));
    infoView.setViewport(sf::FloatRect(worldPart, 0.0f, infoPart, 1.0f));

    viewCenter = (sf::Vector2f)worldSize / 2.0f;
    viewLocation = viewCenter - (worldViewSize / 2.0f);

    worldView.setCenter(viewCenter);

    window.setView(worldView);
};

void RenderState::drawInit() {
    window.clear();
}

void RenderState::drawWorld(World &world, CreatureList &creatureList) {
    window.setView(worldView);

    world.draw(
        window, 
        worldViewSize, viewLocation, drawMode, 
        getSelectedTile()
    );

    creatureList.draw(window, worldViewSize, viewLocation, debug);
}

void RenderState::drawInfoHeader(World &world, CreatureList &creatureList) {

    frameTime = clock.restart();
    totalTime = sf::milliseconds(0);

    fpsMeter.push_back(frameTime);

    if (fpsMeter.size() > FRAMES_MEASURED_FOR_FPS) {
        fpsMeter.pop_front();
    }

    for (sf::Time &i : fpsMeter) {
        totalTime += i;
    }

    perfInfo.setString(
      to_string((float)FRAMES_MEASURED_FOR_FPS / totalTime.asSeconds()) + " FPS"
    );

    perfInfo.setPosition(536 - perfInfo.getLocalBounds().width, 10);


    creatureInfo.setString(    
        to_string(creatureList.count()) + " active creatures, "
      + to_string(creatureList.slotCount()) + " total creatures\n"
    );

    creatureInfo.setPosition(536 - creatureInfo.getLocalBounds().width, 32);


    timeInfo.setString(
        to_string(world.getAge()) 
      + " ticks ("
      + to_string(world.getAge() / UPDATES_PER_YEAR)
      + " years)"
    );


    string updateString = "";

    if (running) {
        updateString += "Running";
    } else {
        updateString += "Paused";
    }

    updateString += " (" + to_string(updatesPerFrame) + " updates per frame)";

    updateInfo.setString(updateString);


    window.draw(perfInfo);
    window.draw(creatureInfo);
    window.draw(timeInfo);
    window.draw(updateInfo);
}

void RenderState::drawInfo(World &world, CreatureList &creatureList) {
    window.setView(infoView);

    drawInfoHeader(world, creatureList);

    if (selected.type == SelectedType::Creature) {
        Creature selectedCreature = creatureList.getCreature(
            selected.creatureID
        );
        if (selectedCreature.getActive()) {
            if (!scrolling) {
                viewCenter = 
                    worldView.getCenter() * 0.9375f
                    + (sf::Vector2f)
                        selectedCreature.getLocation() 
                    * 0.0625f;
                viewLocation = viewCenter - (worldViewSize / 2.0f);
                worldView.setCenter(viewCenter);
            }

            selectedCreature.drawInfo(
                window,
                infoScale,
                font
            );
        } else {
            selected.type = SelectedType::Nothing;
        }
    } else if (selected.type == SelectedType::Tile) {
        Tile selectedTile = world.getTile(
            selected.tileID.x,
            selected.tileID.y
        );
        selectedTile.drawInfo(
            window,
            font
        );
    } else {
        keyInfo.setPosition(10, 70);
        for (int i = 0; i < keyDocumentation.size(); i++) {
            keyInfo.setString(keyDocumentation[i]);
            window.draw(keyInfo);
            keyInfo.move(0, 22);
        }

        creatureList.creatureCountHist.draw(
            window,
            sf::Vector2f(10, 692),
            sf::Vector2f(526, 200)
        );
    }
}

void RenderState::drawFinal() {
    window.display();
}

unsigned int RenderState::getSelectedCreature() {
    return
        selected.type == SelectedType::Creature
      ? selected.creatureID
      : UINT_MAX;
}

sf::Vector2u RenderState::getSelectedTile() {
    return
        selected.type == SelectedType::Tile
      ? selected.tileID
      : (sf::Vector2u){UINT_MAX, UINT_MAX};
}


#include "event_handling.h"
