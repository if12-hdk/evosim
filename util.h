
#include <SFML/Graphics.hpp>

using namespace std;

//struct Selected;

constexpr double pi() {
    return acos(-1.0);
}

float lineSpacing(int textSize, double scale) {
    return (unsigned int)(textSize * LINE_SPACING) * scale;
}

float fitWorldToScreen(
    sf::Vector2u worldSize, sf::Vector2u screenSize, float worldPart
) {
    float displayScale;

    if (
        (float)screenSize.x / worldSize.x * worldPart
      < (float)screenSize.y / worldSize.y
    ) {
        // screen width is limiting factor
        displayScale = (float)screenSize.x / worldSize.x * worldPart;
    } else {
        // screen height is limiting factor
        displayScale = (float)screenSize.y / worldSize.y;
    }

    return displayScale;
}

void textInit(sf::Text &text, sf::Font &font) {
    text.setFont(font);
    text.setCharacterSize(12);
    text.setFillColor(sf::Color::White);
    text.setStyle(sf::Text::Regular);
}

void textInit(
    sf::Text &text, sf::Font &font, 
    unsigned int characterSize, unsigned int x, unsigned int y
) {
    text.setFont(font);
    text.setCharacterSize(characterSize);
    text.setFillColor(sf::Color::White);
    text.setStyle(sf::Text::Regular);
    text.setPosition(x, y);
}

double vectorLength(sf::Vector2<double> vec) {
    return hypot(vec.x, vec.y);
}

sf::Color hsvToRGB(double hue, double saturation, double value) {
    int interval = (int)(hue * 6);
    double colorInInterval = hue * 6 - interval;

    int
        valueNew = (int)(value * 255),
        p = (int)(value * (1 - saturation) * 255),
        q = (int)(value * (1 - saturation * colorInInterval) * 255),
        t = (int)(value * (1 - saturation * (1 - colorInInterval)) * 255);

    //cout << valueNew << " " << p << " " << q << " " << t << "\n";

    switch (interval) {
        case 0:
        case 6:
            return sf::Color(valueNew, t, p);
            break;
        case 1:
            return sf::Color(q, valueNew, p);
            break;
        case 2:
            return sf::Color(p, valueNew, t);
            break;
        case 3:
            return sf::Color(p, q, valueNew);
            break;
        case 4:
            return sf::Color(t, p, valueNew);
            break;
        case 5:
            return sf::Color(valueNew, p, q);
            break;
    }
}

sf::Vector2f screenToWorld(sf::Vector2f topleft, sf::Vector2f screenPoint) {
    sf::Vector2f worldPoint;
    worldPoint.x = screenPoint.x + topleft.x;
    worldPoint.y = screenPoint.y + topleft.y;
    return worldPoint;
}

sf::Vector2f worldToScreen(sf::Vector2f topleft, sf::Vector2f worldPoint) {
    sf::Vector2f screenPoint;
    screenPoint.x = worldPoint.x - topleft.x;
    screenPoint.y = worldPoint.y - topleft.y;
    return screenPoint;
}

sf::Vector2<double> rotateByAngle(sf::Vector2<double> original, double angle) {
    float oldX = original.x, oldY = original.y;
    float
        newX = cos(angle) * oldX - sin(angle) * oldY,
        newY = cos(angle) * oldY + sin(angle) * oldX;

    return {newX, newY};

}

sf::Vector2f rotateByAngle(sf::Vector2f original, float angle) {
    float oldX = original.x, oldY = original.y;
    float
        newX = cos(angle) * oldX - sin(angle) * oldY,
        newY = cos(angle) * oldY + sin(angle) * oldX;

    return {newX, newY};

}