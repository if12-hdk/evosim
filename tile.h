#include <vector>
#include <utility>
#include <cmath>

#include <SFML/Graphics.hpp>

#define ENERGY_PER_STEP 0.0006
#define ENERGY_LOSS 0.0004

using namespace std;

enum class DrawMode {
    drawOriginal,
    drawImproved,
    drawElevation,
    drawHumidity,
    drawTemperature,
    drawEnergy
};

class Tile {
    unsigned int x, y;
    double 
        energy, humidity, temperature, 
        elevation, 
        amountEaten, lastAmountEaten, totalEaten, deadMatter;
    double currentTemperature;
    double tempHumidity;
    array<double, 8> humiditySpread;
    sf::Vector2f wind;

public:
    void init(
        unsigned int x, unsigned int y,
        double elevation, double humidity, double temperature,
        array<double, 8> humiditySpread
        );

    sf::Vector2u getLocation();
    double const &getEnergy();
    double const &getElevation();
    double const &getTemperature();
    double const &getHumidity();

    void increaseAmountEaten(double amount);

    void update(double timeOfYear);
    void updateClimate(Tile neighbors[8]);
    void setClimate();

    sf::Color calculateColor(DrawMode mode);

    void debugInfo();
    void drawInfo(sf::RenderWindow &window, sf::Font &font);
};

void Tile::init(
    unsigned int x, unsigned int y,
    double elevation, double humidity, double temperature,
    array<double, 8> humiditySpread
) {
    this->x = x;
    this->y = y;
    this->elevation = elevation;
    this->humidity = humidity;
    this->temperature = temperature;
    this->humiditySpread = humiditySpread;
    this->energy = 0;
    this->amountEaten = 0;
    this->totalEaten = 0;
    this->deadMatter = 0;
}

sf::Vector2u Tile::getLocation() {
    return sf::Vector2u(x, y);
}

double const &Tile::getEnergy() {
    return energy;
}

double const &Tile::getElevation() {
    return elevation;
}

double const &Tile::getTemperature() {
    return temperature;
}

double const &Tile::getHumidity() {
    return humidity;
}

void Tile::increaseAmountEaten(double amount) {
    amountEaten += amount;
}

void Tile::update(double timeOfYear) {
    currentTemperature = min(1.0, max(
        0.0, temperature + 0.3 * humidity * sin(timeOfYear)
    ));

    energy = max(0.0, energy - amountEaten);
    energy += (currentTemperature - 0.35)
           * humidity * ENERGY_PER_STEP * (1 - deadMatter);

    energy *= (1 - ENERGY_LOSS);

    /*if (energy > 1) {
        cout << currentTemperature << " " << humidity << "\n";
        cout << (currentTemperature - 0.35) * humidity * ENERGY_PER_STEP
             << "\n";
        cout << energy << "\n";
        cout << amountEaten << "\n";
    }*/
    energy = max(0.0, min(energy, 1.0));
    lastAmountEaten = amountEaten;
    totalEaten += amountEaten;
    deadMatter += amountEaten * 2;
    deadMatter *= DEAD_MATTER_DECLINE;
    amountEaten = 0;
}

void Tile::updateClimate(Tile neighbors[8]) {
    double actualSpread;
    tempHumidity = 0;

    for (int i = 0; i < 8; i++) {
        actualSpread = 0.1
            * pow((elevation / neighbors[i].elevation), 2)
            * humiditySpread[i];

        tempHumidity += neighbors[i].humidity * actualSpread;
        tempHumidity += humidity * (0.125 - actualSpread);
    }
    /*tempHumidity = (
        neighbors[0].humidity * 0.0075 + neighbors[1].humidity * 0.005 + neighbors[2].humidity * 0.003
      + neighbors[3].humidity * 0.02 + humidity * 0.95 + neighbors[4].humidity * 0.004
      + neighbors[5].humidity * 0.0075 + neighbors[6].humidity * 0.005 + neighbors[7].humidity * 0.003
      );*/
}

void Tile::setClimate() {
    if (humidity < 1 && tempHumidity < 1) {
        humidity = tempHumidity;
    }
}

sf::Color Tile::calculateColor(DrawMode mode) {
    switch(mode) {
    case DrawMode::drawOriginal:
        if (elevation <= 1.0 && elevation >= 0.0) {
            return sf::Color(
                /*(int)(elevation * 255)*/255 - (int)(humidity * 255),
                /*(int)(elevation * 255) / 2 + */(int)(temperature * 255),
                (int)humidity * 255
            );
        }
        else {
            return sf::Color::Red;
        }
        break;
    case DrawMode::drawImproved:
        if (elevation <= 1.0 && elevation >= 0.0) {
            return hsvToRGB(
                (1.0 + ((humidity == 1) ? 1.0 : 0.0)) / 6 + humidity / 4,
                temperature,
                WATER_LEVEL + (1 - elevation) * (1 - WATER_LEVEL)
            );
        }
        else {
            return sf::Color::Red;
        }
        break;
    case DrawMode::drawElevation:
        return sf::Color(
            (int)(elevation * 255),
            (int)(elevation * 255),
            (int)(elevation * 255)
        );
        break;
    case DrawMode::drawHumidity:
        return sf::Color(
            (int)(humidity * 255),
            (int)(humidity * 255),
            (int)(humidity * 255)
        );
        break;
    case DrawMode::drawTemperature:
        return sf::Color(
            (int)(temperature * 255),
            (int)(temperature * 255),
            (int)(temperature * 255)
        );
        break;
    case DrawMode::drawEnergy:
        return sf::Color(
            (int)(energy * 255),
            (int)(energy * 255),
            (int)(energy * 255)
        );
        break;
    }
}

void Tile::drawInfo(sf::RenderWindow &window, sf::Font &font) {
    sf::Text info;
    textInit(info, font, 14, 10, 70);

    vector<string> infoValues = {
        "Tile at x:" + to_string(x) + ", y:" + to_string(y),
        "energy: " + to_string(energy),
        "amount eaten: " + to_string(lastAmountEaten),
        "total amount eaten: " + to_string(totalEaten),
        "dead matter: " + to_string(deadMatter),
        "elevation: " + to_string(elevation),
        "humidity: " + to_string(humidity),
        "temperature: " + to_string(temperature)
    };

    for (string value : infoValues) {
        info.setString(value);
        window.draw(info);
        info.move(0, 22);
    }
}