#ifndef NO_IO
#include <iostream>
#endif
#include <vector>
#include <utility>
#include <cmath>
#include <random>
#include <climits>
#include <sstream>
#include <iomanip>

#include <SFML/Graphics.hpp>

#include "brain.h"

using namespace std;

enum class ParentsType {
    primordial = 0,
    single = 1,
    two = 2
};

struct Parents {
    ParentsType type;
    union {
        bool noParent;
        unsigned long int oneParent;
        array<unsigned long int, 2> twoParents;
    };
};

struct UpdateStatus {
    bool alive;
    bool givingBirth;
    double fighting = 0;
};

class Creature {
    bool active = false;
    bool isSelected = false;

    unsigned long int creatureID;
    unsigned long int age;
    Parents parents;
    double energy, size;

    double currentFighting = 0;

    unsigned int selectedTime = 0;

    sf::Vector2<double> location;
    double direction, speed, acceleration;

    double eleAtLocation, eleAtSensor;

    double
       attackAbility, walkAbility, swimAbility, grazeAbility,
       heatResistance, coolResistance;

    double bodyHue = 0;
    sf::Color bodyColor {0, 0, 0};

    double sensorDistance, sensorAngle;

    CreatureInput currentInput;

    Brain brain;

public:
    void init();

    void randomInit(
        unsigned long int creatureID,
        BrainType brainType,
        default_random_engine &randomEngine,
        uniform_real_distribution<> &randomXCoord,
        uniform_real_distribution<> &randomYCoord
    );
    void cloneInit(
        World &world,
        unsigned long int creatureID,
        sf::Vector2<double> location,
        Brain parentBrain,
        unsigned long int parentID,
        default_random_engine &randomEngine
    );
    void twoParentInit(
        unsigned long int creatureID,
        sf::Vector2<double> location,
        Brain parentBrainOne,
        Brain parentBrainTwo
    );

    void deactivate();

    unsigned long int const &getID();
    double const &getEnergy();
    unsigned long int const &getAge();
    double const &getBodyHue();
    double const &getSize();

    sf::Vector2<double> const getSensorLocation();
    sf::Vector2<double> const &getLocation();

    Brain const &getBrain();


    bool isVisible(sf::Vector2f viewSize, sf::Vector2f viewLocation);
    bool getActive();

    void getValues(World &world, bool canSeeCreature, double seenCreatureHue);
    UpdateStatus applyUpdate(World &world, bool selected);

    void draw(sf::RenderWindow &window);
    void drawInfo(
        sf::RenderWindow &window,
        double scale,
        sf::Font &font
    );
    void drawArrayBackground(
        sf::RenderWindow &window,
        array<uint8_t, 256> &drawnArray,
        sf::Color color,
        const sf::Vector2f &offset,
        const sf::Vector2f &scale
    );
    void drawArrayBackground(
        sf::RenderWindow &window,
        array<uint8_t, 256> &drawnArrayOne,
        sf::Color colorOne,
        array<uint8_t, 256> &drawnArrayTwo,
        sf::Color colorTwo,
        const sf::Vector2f &offset,
        const sf::Vector2f &scale
    );
    void drawArray(
        sf::RenderWindow &window,
        array<uint8_t, 256> &drawnArray,
        const sf::Vector2f &offset,
        const sf::Vector2f &scale,
        sf::Font &font, int textScale
    );
};

void Creature::init() {
    //this->creatureID = creatureID;
    //this->brain = brain;
    //this->location = location;
    active = true;
    age = 0;
    size = STARTING_SIZE;
    energy = STARTING_ENERGY;
    sensorDistance = 0.1;

    speed = 0.0;
    acceleration = 0.0;

    attackAbility = 0.1;
    grazeAbility = 0.1;
    walkAbility = STARTING_WALKING_SPEED;
    swimAbility = STARTING_SWIMMING_SPEED;
    heatResistance = 0.1;
    coolResistance = 0.1;
}

void Creature::randomInit(
    unsigned long int creatureID,
    BrainType brainType,
    default_random_engine &randomEngine,
    uniform_real_distribution<> &randomXCoord,
    uniform_real_distribution<> &randomYCoord
) {
    uniform_real_distribution<> baseDistribution;

    this->creatureID = creatureID;
    brain.type = brainType;

    switch (brainType) {
    case BrainType::subleqBrain:
        uniform_int_distribution<uint8_t> distribution(0, 255);
        array<uint8_t, SubleqMachine::programSize> program;

        for (unsigned int i = 0; i < SubleqMachine::programSize; i++) {
            program[i] = distribution(randomEngine);
        }

        brain.initSubleq(program);

        //cout << to_string(program[5]) << "\n";
        break;
    }

    this->location = {
        randomXCoord(randomEngine),
        randomYCoord(randomEngine),
    };

    this->direction = baseDistribution(randomEngine) * 2 * pi();

    //cout << this->location.x << " " << this->location.y << "\n";

    this->parents.type = ParentsType::primordial;
    this->parents.noParent = true;

    this->init();
}

void Creature::cloneInit(
    World &world,
    unsigned long int creatureID,
    sf::Vector2<double> location,
    Brain parentBrain,
    unsigned long int parentID,
    default_random_engine &randomEngine
) {
    uniform_int_distribution<uint8_t> valueDistribution(0, 255);
    uniform_real_distribution<> baseDistribution(0, 1);

    sf::Vector2<double> offset;

    offset = rotateByAngle(
        {0.0, baseDistribution(randomEngine)},
        baseDistribution(randomEngine) * 2 * pi()
    );

    this->creatureID = creatureID;

    this->location = world.clampToBoundary(location + offset, 0.1);
    this->direction = baseDistribution(randomEngine) * 2 * pi();

    this->brain = parentBrain;

    for (int i = 0; i < 256; i++) {
        if (baseDistribution(randomEngine) < MUTATION_CHANGES){
            brain.subleqBrain.program[i] = valueDistribution(randomEngine);
        }
    }


    //cout << this->location.x << " " << this->location.y << "\n";

    this->parents.type = ParentsType::single;
    this->parents.oneParent = parentID;

    this->init();
}

void Creature::deactivate() {
    active = false;
}

unsigned long int const &Creature::getID() {
    return creatureID;
}


double const &Creature::getEnergy() {
    return energy;
}


unsigned long int const &Creature::getAge() {
    return age;
}


double const &Creature::getBodyHue() {
    return bodyHue;
}

double const &Creature::getSize() {
    return size;
}

sf::Vector2<double> const Creature::getSensorLocation() {
    return location + rotateByAngle(
        {0.0, -sensorDistance},
        sensorAngle + direction
    );
}

sf::Vector2<double> const &Creature::getLocation() {
    return location;
}

Brain const &Creature::getBrain() {
    return brain;
}

bool Creature::isVisible(sf::Vector2f viewSize, sf::Vector2f viewLocation) {
    return (
        (location.x > viewLocation.x - size)
     && (location.y > viewLocation.y - size)
     && (location.x < viewLocation.x + viewSize.x + size)
     && (location.y < viewLocation.y + viewSize.y + size)
    );
}

bool Creature::getActive() {
    return active;
}

void Creature::getValues(
    World &world,
    bool canSeeCreature,
    double seenCreatureHue
) {
    Tile tileUnderCreature, tileUnderSensor;
    sf::Vector2<double> sensorLocation;

    eleAtLocation = world.elevationAtPoint(location);
    eleAtSensor = world.elevationAtPoint(sensorLocation);
    //unsigned int seenCreatureID;

    currentInput.bodyHue = bodyHue;
    currentInput.energy = energy;
    currentInput.size = size;
    currentInput.speed = speed;

    sensorLocation = getSensorLocation();

    tileUnderCreature = world.getTile(
        (int)location.x,
        (int)location.y
    );

    if (world.pointInBounds(sensorLocation)) {
        tileUnderSensor = world.getTile(
            (int)sensorLocation.x,
            (int)sensorLocation.y
        );
    } else {
        tileUnderSensor.init(
            0.0, 0.0, 0.5, 0.0, 0.0,
            {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
        );
    }

    currentInput.tileEnergy = tileUnderCreature.getEnergy();
    currentInput.tileTemperature = tileUnderCreature.getTemperature();
    currentInput.tileElevation = eleAtLocation;

    currentInput.seenTileType = 
        eleAtSensor > WATER_LEVEL 
      ? TileType::land 
      : TileType::water;
    /*Slope = (
        eleAtSensor
      - eleAtLocation
    ) / sensorDistance;*/

    //cout << currentInput.seenTileSlope << "\n";

    currentInput.seenTileEnergy = tileUnderSensor.getEnergy();

    //seenCreatureID = creatureList.creatureAtPosition(sensorLocation, 0.0);
    if (canSeeCreature) {
        currentInput.canSeeCreature = true;
        currentInput.creatureBodyHue = seenCreatureHue;
    } else {
        currentInput.canSeeCreature = false;
        currentInput.creatureBodyHue = 0.0;
    }
}

UpdateStatus Creature::applyUpdate(World &world, bool selected) {
    Tile &tileUnderCreature = world.getTile(
        (int)location.x,
        (int)location.y
    );

    CreatureAction result;
    UpdateStatus ret {true, false};
    sf::Vector2<double> newLocation;

    if (!isSelected) {
        selectedTime = 0;
    }

    isSelected = selected;

    result = brain.run(currentInput, selected);


    if (brain.type == BrainType::subleqBrain) {
        bodyHue = (double)brain.subleqBrain.program[0] / 256;
        //cout << bodyHue << " " << result.hue << "\n";
    }

    acceleration = result.acceleration;
    if (eleAtLocation < WATER_LEVEL) { // water
        acceleration *= swimAbility;
        swimAbility = min(
            1.0,
            swimAbility + abs(acceleration) * MOVEMENT_IMPROVEMENT
        );
        walkAbility = max(
            0.001,
            walkAbility 
          - abs(acceleration) * MOVEMENT_IMPROVEMENT * MOVEMENT_SPECIALIZATION
        );

        energy -= abs(acceleration) * WATER_MOVEMENT_COST;

    } else { // land
        acceleration *= walkAbility;
        walkAbility = min(
            1.0,
            walkAbility + abs(acceleration) * MOVEMENT_IMPROVEMENT
        );
        swimAbility = max(
            0.001,
            swimAbility 
          - abs(acceleration) * MOVEMENT_IMPROVEMENT * MOVEMENT_SPECIALIZATION
        );

        energy -= abs(acceleration) * LAND_MOVEMENT_COST;
    }

    energy -= GRAZING_COST * result.grazing;

    energy += 
        tileUnderCreature.getEnergy() * grazeAbility * ENERGY_FROM_GROUND
      * result.grazing;

    tileUnderCreature.increaseAmountEaten(
        tileUnderCreature.getEnergy()
      * ENERGY_FROM_GROUND
      * result.grazing
      / DEAD_MATTER_GROWTH_RATIO
    );

    energy -= METABOLISM;

    if (result.givingBirth > 0.5 && energy > MINIMUM_BIRTH_ENERGY) {
        ret.givingBirth = true;
        energy -= BIRTH_COST;
    }

    currentFighting = 
        result.fighting > 0.5
          ? result.fighting
          : 0;


    ret.fighting = currentFighting * FIGHTING_ENERGY;

    energy -= currentFighting * FIGHTING_COST;

    //cout << energy << "\n";

    if (energy < 0.0) {
        //cout << "ok" << endl;
        return {false, false, 0.0};
    }


    speed += acceleration / 100;
    speed = speed * 0.9;

    direction += result.angleChange / 5;
    direction = fmod(direction + 2 * pi(), 2 * pi());

    location = world.clampToBoundary(
        location + rotateByAngle(
            (sf::Vector2<double>){0.0, -speed},
            direction
        ),
        size
    );

    sensorDistance = result.sensorDistance;
    sensorAngle = result.sensorAngle;

    //cout << "sensorDistance: " << result.sensorDistance << "\n";
    //cout << "sensorAngle: " << result.sensorAngle << "\n";

    ++age;

    if (selected) {
        ++selectedTime;
    }

    return ret;
}

void Creature::draw(sf::RenderWindow &window) {
    sf::CircleShape body, sensor, fightRing;

    float ringRadius = size + currentFighting * size * FIGHT_RANGE;

    body.setRadius(size);
    fightRing.setRadius(ringRadius);
    body.setOrigin({(float)size, (float)size});
    fightRing.setOrigin(
        {ringRadius, ringRadius}
    );
    body.setPosition((sf::Vector2f)location);
    fightRing.setPosition((sf::Vector2f)location);

    bodyColor = hsvToRGB(bodyHue, 1, 1);

    switch (brain.type){
    case BrainType::subleqBrain:
        body.setFillColor(bodyColor);
    }

    fightRing.setFillColor({255, 0, 0, 100});

    if (isSelected) {
        body.setOutlineThickness(size / 2.5);
        body.setOutlineColor(sf::Color::White);
    } else {
        body.setOutlineThickness(size / 25);
        body.setOutlineColor(sf::Color::Black);
    }

    sensor.setRadius(0.02);
    sensor.setOrigin({0.02f, 0.02f});
    sensor.setPosition((sf::Vector2f)getSensorLocation());
    //cout << location.x << " " << location.y << "\n";
    //cout << getSensorLocation().x << " " << getSensorLocation().y << "\n";

    sensor.setFillColor(sf::Color::Black);

    window.draw(fightRing);

    window.draw(body);

    window.draw(sensor);
}

void Creature::drawArrayBackground(
    sf::RenderWindow &window,
    array<uint8_t, 256> &drawnArray,
    sf::Color defaultColor,
    const sf::Vector2f &offset, const sf::Vector2f &scale
) {
    double alpha;
    uint8_t maxValue = 0;
    for (uint8_t i : drawnArray) {
        maxValue = max(i, maxValue);
    }

    //cout << maxValue << "\n";

    sf::RectangleShape coloredSquare {scale};

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            alpha = (double)drawnArray[i * 16 + j] / maxValue;
            //cout << alpha << "\n";
            coloredSquare.setFillColor(
                defaultColor
              * sf::Color(255, 255, 255, alpha * 255)
            );
            coloredSquare.setPosition(
                offset.x + j * scale.x,
                offset.y + i * scale.y
            );

            window.draw(coloredSquare);
        }
    }
}

void Creature::drawArrayBackground(
    sf::RenderWindow &window,
    array<uint8_t, 256> &drawnArrayOne, sf::Color colorOne,
    array<uint8_t, 256> &drawnArrayTwo, sf::Color colorTwo,
    const sf::Vector2f &offset, const sf::Vector2f &scale
) {
    /*for (int i : drawnArrayOne) {
        cout << i << " ";
    }
    cout << "\n";
    for (int i : drawnArrayTwo) {
        cout << i << " ";
    }
    cout << "\n";*/

    double alphaOne, alphaTwo;
    uint8_t maxValueOne = 0, maxValueTwo = 0;

    for (uint8_t o : drawnArrayOne) {
        maxValueOne = max(o, maxValueOne);
    }
    for (uint8_t t : drawnArrayTwo) {
        maxValueTwo = max(t, maxValueTwo);
    }

    //cout << maxValue << "\n";

    sf::RectangleShape coloredSquare {scale};

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            alphaOne = (double)drawnArrayOne[i * 16 + j] / maxValueOne;
            alphaTwo = (double)drawnArrayTwo[i * 16 + j] / maxValueTwo;
            //cout << alphaOne << " " << alphaTwo << "\n";


            coloredSquare.setFillColor(
                (colorOne * sf::Color(
                    alphaOne * 255, alphaOne * 255, alphaOne * 255
                ))
              + (colorTwo * sf::Color(
                    alphaTwo * 255, alphaTwo * 255, alphaTwo * 255
                ))
            );
            coloredSquare.setPosition(
                offset.x + j * scale.x,
                offset.y + i * scale.y
            );

            window.draw(coloredSquare);
        }
    }
}

void Creature::drawArray(
    sf::RenderWindow &window,
    array<uint8_t, 256> &drawnArray,
    const sf::Vector2f &offset, const sf::Vector2f &scale,
    sf::Font &font, int textScale
) {
    sf::Text text;
    textInit(text, font, textScale, 0, 0);
    stringstream stream;

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            stream
                << setfill('0')
                << setw(2)
                << hex;

            stream << (int)drawnArray[i * 16 + j];
            //cout << stream.str() << "\n";

            text.setString(stream.str());
            stream.str("");

            text.setPosition(offset.x + j * scale.x, offset.y + i * scale.y);

            window.draw(text);

            //stream.flush();
        }
    }
}

void Creature::drawInfo(
    sf::RenderWindow &window,
    double scale,
    sf::Font &font
) {
    isSelected = true;
    sf::Text info;
    textInit(info, font, LARGE_TEXT_SIZE * scale, 10, 70);

    float smallLineSpacing = lineSpacing(SMALL_TEXT_SIZE, scale);
    float mediumLineSpacing = lineSpacing(MEDIUM_TEXT_SIZE, scale);
    float largeLineSpacing = lineSpacing(LARGE_TEXT_SIZE, scale);

    sf::Vector2f smallSpacingSquare = {smallLineSpacing, smallLineSpacing};

    vector<string> infoValues = {
        "parents: " + 
        (parents.type == ParentsType::primordial ? "none" 
      : parents.type == ParentsType::single ? to_string(parents.oneParent)
      : to_string(parents.twoParents[0]) 
      + " and " 
      + to_string(parents.twoParents[1])
        ),

        "age: " + to_string(age),
        "energy: " + to_string(energy),
        "speed: " + to_string(speed),
        "acceleration: " + to_string(acceleration),
        "direction: " + to_string(direction),
        "attack strength: " + to_string(attackAbility),
        "walking speed: " + to_string(walkAbility),
        "swimming speed: " + to_string(swimAbility),
        "grazing ability: " + to_string(grazeAbility),
        "heat resistance: " + to_string(heatResistance),
        "cold resistance: " + to_string(coolResistance)
    };

    if (brain.type == BrainType::subleqBrain) {
        if (selectedTime != 0){
            drawArrayBackground(
                window, brain.subleqBrain.programHeatmap,
                {255, 0, 0},
                sf::Vector2f{234.0f, 70.0f}
              - smallSpacingSquare / 10.0f,
                smallSpacingSquare
            );
            drawArrayBackground(
                window,
                brain.subleqBrain.dataReadHeatmap, {0, 0, 255},
                brain.subleqBrain.dataWriteHeatmap, {0, 255, 0},
                sf::Vector2f{234.0f, 390.0f}
              - smallSpacingSquare / 10.0f,
                smallSpacingSquare
            );
            //cout << selectedTime << "\n";
        }

        drawArray(
            window,
            brain.subleqBrain.program,
            sf::Vector2f{234.0f, 70.0f},
            smallSpacingSquare,
            font, SMALL_TEXT_SIZE * scale
        );

        drawArray(
            window,
            brain.subleqBrain.data,
            sf::Vector2f{234.0f, 390.0f},
            smallSpacingSquare,
            font, SMALL_TEXT_SIZE * scale
        );
    }

    info.setString("Creature #" + to_string(creatureID));
    window.draw(info);
    info.move(0, largeLineSpacing);
    info.setCharacterSize((int)(MEDIUM_TEXT_SIZE * scale));

    for (string value : infoValues) {
        info.setString(value);
        window.draw(info);
        info.move(0, mediumLineSpacing);
    }
}
