#include <vector>
#include <utility>
#include <climits>
#include <cmath>

#include <SFML/Graphics.hpp>

#include "tile.h"

#include "opensimplexnoise/opensimplexnoise.hpp"

using namespace std;

class World {
    static const int
        width = WIDTH, height = HEIGHT,
        octaveScaling = 2, octaveZooming = 2, octaveCount = 5;

    unsigned long long int age;

    const float
        initialZoom = 0.12f,
        initialScale = (float)(octaveScaling - 1)
                     / ((pow(octaveScaling, octaveCount) - 1) * 2);

    array<array<Tile, height>, width> tiles;
    vector<OSN::Noise<4>> noises;

public:
    void init(int seed);
    const int getWidth();
    const int getHeight();
    const int getAge();

    bool pointInBounds(sf::Vector2f point);
    bool pointInBounds(sf::Vector2u point);
    bool pointInBounds(sf::Vector2<double> point);
    bool pointInBounds(sf::Vector2<double> point, double tolerance);
    double elevationAtPoint(sf::Vector2<double> point);
    double elevationAtPoint(sf::Vector2f point);

    sf::Vector2<double> clampToBoundary(
        sf::Vector2<double> point,
        double tolerance
    );

    Tile &getTile(int xCoord, int yCoord);
    void update();
    void draw(
        sf::RenderWindow &window,
        sf::Vector2f screenSize, sf::Vector2f topleft, DrawMode mode,
        sf::Vector2u selectedTile
    );
};


void World::init(int seed) {

    double elevation, temperature, humidity;
    double scale, zoom;
    sf::Vector2f starterPoint {1, 0};
    sf::Vector2f xyCircle, zwCircle;

    array<double, 8> humiditySpread = {
        0.25, 0.05, 0.05,
        0.7,        0.05,
        0.25, 0.05, 0.05
    };

    for (int i = 0; i < octaveCount; i++) {
        noises.push_back(OSN::Noise<4>(seed));
    }

    for (int i = 0; i < width; i++) {
        //vector.push_back(vector<tile>)
        for (int j = 0; j < height; j++) {
            xyCircle = rotateByAngle(
                starterPoint,
                (pi() * 2) * (float)i / width
            );
            zwCircle = rotateByAngle(
                starterPoint,
                (pi() * 2) * (float)j / height
            );
            elevation = 0.5;

            for (int k = 0; k < noises.size(); k++) {
                scale = initialScale * pow(octaveScaling, k);
                zoom = initialZoom * pow(octaveZooming, k);
                elevation += scale * noises[k].eval(
                    xyCircle.x / zoom,
                    xyCircle.y / zoom,
                    zwCircle.x / zoom,
                    zwCircle.y / zoom
                );
            }
            temperature = 0.75;
            temperature -= elevation / 2;

            humidity = (elevation > WATER_LEVEL) ? 0.0 : 1.0;

            //cout << elevation << " " << humidity << endl;

            tiles[i][j].init(i, j, elevation, humidity, temperature, humiditySpread);
        }
        //cout << (int)((-cos(pi() * 2 * i / width) + 1) / 2 * 120) << endl;
    }

    //cout << (0 + width - 1) % width << "\n";

    for (int k = 0; k < 100; k++) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                //cout << i << " " << j << " " << k << "\n";
                //cout << max(0, i - 1) << " " << max(0, j - 1) << "\n";

                Tile neighbors[8] {
                    tiles[(i + width - 1) % width][(j + height - 1) % height],
                    tiles[i                      ][(j + height - 1) % height],
                    tiles[(i + 1) % width        ][(j + height - 1) % height],
                    tiles[(i + width - 1) % width][j],
                    tiles[(i + 1) % width        ][j],
                    tiles[(i + width - 1) % width][(j + 1) % height],
                    tiles[i                      ][(j + 1) % height],
                    tiles[(i + 1) % width        ][(j + 1) % height],
                };
                tiles[i][j].updateClimate(neighbors);
            }
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                tiles[i][j].setClimate();
            }
        }

    }

}

const int World::getWidth() {
    return WIDTH;
}

const int World::getHeight() {
    return HEIGHT;
}

const int World::getAge() {
    return age;
}

bool World::pointInBounds(sf::Vector2f point) {
    return point.x >= 0 && point.x <= width
        && point.y >= 0 && point.y <= height;
}

bool World::pointInBounds(sf::Vector2u point) {
    return point.x >= 0 && point.x <= width
        && point.y >= 0 && point.y <= height;
}

bool World::pointInBounds(sf::Vector2<double> point) {
    return point.x >= 0 && point.x <= width
        && point.y >= 0 && point.y <= height;
}

bool World::pointInBounds(sf::Vector2<double> point, double tolerance) {
    return point.x >= tolerance && point.x <= width - tolerance
        && point.y >= tolerance && point.y <= height - tolerance;
}

double World::elevationAtPoint(sf::Vector2<double> point) {
    sf::Vector2<double> adjPoint = point - sf::Vector2<double>({0.5, 0.5});
    adjPoint.x = fmod(adjPoint.x + width, width);
    adjPoint.y = fmod(adjPoint.y + height, height);

    if (!pointInBounds(point)) {
        return 0;
    }

    unsigned int
        x = (unsigned int)adjPoint.x,
        y = (unsigned int)adjPoint.y;

    //cout << x << " " << y << "\n";
    //point += {0.5, 0.5};
    if ((adjPoint.x - x == 0.0) && (adjPoint.y - y == 0.0)) {
        return tiles[x][y].getElevation();

    } else if (adjPoint.x - x == 0.0) {
        return
            tiles[x][y].getElevation() * (1.0 - (adjPoint.y - y))
          + tiles[x][(y + 1) % height].getElevation() * (adjPoint.y - y);
    } else if (adjPoint.y - y == 0.0) {
        return
            tiles[x][y].getElevation() * (1.0 - (adjPoint.x - x))
          + tiles[(x + 1) % width][y].getElevation() * (adjPoint.x - x);
    } else {
        return
            tiles[x][y].getElevation()
              * (1.0 - (adjPoint.x - x)) * (1.0 - (adjPoint.y - y))
          + tiles[(x + 1) % width][y].getElevation()
              * (adjPoint.x - x) * (1.0 - (adjPoint.y - y))
          + tiles[x][(y + 1) % height].getElevation()
              * (1.0 - (adjPoint.x - x)) * (adjPoint.y - y)
          + tiles[(x + 1) % width][(y + 1) % height].getElevation()
              * (adjPoint.x - x) * (adjPoint.y - y);
    }
}

double World::elevationAtPoint(sf::Vector2f point) {
    sf::Vector2f adjPoint = point - sf::Vector2f({0.5, 0.5});
    adjPoint.x = fmod(adjPoint.x + width, width);
    adjPoint.y = fmod(adjPoint.y + height, height);

    if (!pointInBounds(point)) {
        return 0;
    }
    unsigned int
        x = (unsigned int)adjPoint.x,
        y = (unsigned int)adjPoint.y;
    //cout << x << " " << y << "\n";
    //point += {0.5, 0.5};
    if ((adjPoint.x - x == 0.0f) && (adjPoint.y - y == 0.0f)) {
        return tiles[x][y].getElevation();

    } else if (adjPoint.x - x == 0.0f) {
        return
            tiles[x][y].getElevation() * (1.0f - (adjPoint.y - y))
          + tiles[x][(y + 1) % height].getElevation() * (adjPoint.y - y);
    } else if (adjPoint.y - y == 0.0f) {
        return
            tiles[x][y].getElevation() * (1.0f - (adjPoint.x - x))
          + tiles[(x + 1) % width][y].getElevation() * (adjPoint.x - x);
    } else {
        return
            tiles[x][y].getElevation()
              * (1.0f - (adjPoint.x - x)) * (1.0f - (adjPoint.y - y))
          + tiles[(x + 1) % width][y].getElevation()
              * (adjPoint.x - x) * (1.0f - (adjPoint.y - y))
          + tiles[x][(y + 1) % height].getElevation()
              * (1.0f - (adjPoint.x - x)) * (adjPoint.y - y)
          + tiles[(x + 1) % width][(y + 1) % height].getElevation()
              * (adjPoint.x - x) * (adjPoint.y - y);
    }
}

sf::Vector2<double> World::clampToBoundary(
    sf::Vector2<double> point,
    double tolerance
) {
    if (point.x <= tolerance) {
        point.x = tolerance;
    } else if (point.x >= width - tolerance) {
        point.x = width - tolerance;
    }
    if (point.y <= tolerance) {
        point.y = tolerance;
    } else if (point.y >= height - tolerance) {
        point.y = width - tolerance;
    }
    return point;
}

Tile &World::getTile(int xCoord, int yCoord) {
    // only call with xCoord and yCoord within boundaries
    return tiles[xCoord][yCoord];
}

void World::update() {
    double timeOfYear;

    timeOfYear = (double)(age % UPDATES_PER_YEAR) / UPDATES_PER_YEAR * 2 * pi();
    //cout << age << " " << timeOfYear << "\n";
    //cout << sin(timeOfYear) << "\n";
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            tiles[i][j].update(timeOfYear);
        }
    }

    ++age;
}

void World::draw(
    sf::RenderWindow &window,
    sf::Vector2f screenSize, sf::Vector2f topleft, DrawMode drawMode,
    sf::Vector2u selectedTile
) {
    //cout << topleft.x << " " << topleft.y << endl;
    //cout << screenSize.x << " " << screenSize.y << endl;

    int minX, maxX, minY, maxY, i, j;
    int worldWidth = getWidth();
    int worldHeight = getHeight();

    sf::Vector2f bottomright, topleftTile, currentTile;
    //vector<sf::RectangleShape> result;

    bottomright = topleft + screenSize;

    topleftTile = {max(floor(topleft.x), 0.0f), max(floor(topleft.y), 0.0f)};

    //offset = {topleftTile.x % 1, topleftTile.y % 1}

    //cout << topleftTile.x << " " << topleftTile.y << endl;

    currentTile = topleftTile;

    minX = max(0, (int)topleft.x);
    maxX = min(worldWidth, (int)bottomright.x + 1);
    minY = max(0, (int)topleft.y);
    maxY = min(worldHeight, (int)bottomright.y + 1);

    //cout << ((maxX - minX) * (maxY - minY)) << endl;

    //result.reserve(max((maxX - minX) * (maxY - minY), 0));

    for (i = minX; i < maxX; i++) {
        //cout << ((i % worldWidth + worldWidth) % worldWidth) << "\n";

        for (j = minY; j < maxY; j++) {

                sf::RectangleShape tileToRender({1, 1});
                tileToRender.setPosition(currentTile);

                tileToRender.setFillColor(
                    tiles
                        [i]
                        [j]
                        .calculateColor(drawMode)
                );

                //result.push_back(tileToRender);
                window.draw(tileToRender);

                currentTile.y += 1;
        }
        currentTile.x += 1;
        currentTile.y = topleftTile.y;
    }

    if (selectedTile.x != UINT_MAX) {
        sf::RectangleShape selectedOutline({1, 1});

        selectedOutline.setFillColor(sf::Color::Transparent);

        selectedOutline.setOutlineColor(sf::Color::White);
        selectedOutline.setOutlineThickness(0.05);
        selectedOutline.setPosition((sf::Vector2f)selectedTile);

        window.draw(selectedOutline);
    }

    //return result;
}
