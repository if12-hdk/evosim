You need SFML 2.5.1 for this project, download at https://www.sfml-dev.org/download/sfml/2.5.1/

Place it directly inside the main folder. (It does not come bundled due to its size.)