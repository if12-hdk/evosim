#include <SFML/Graphics.hpp>
#include <cmath>

using namespace std;

enum class SelectedType {
    Creature,
    Tile,
    Nothing
};

struct Selected {
    SelectedType type;
    union {
        unsigned int creatureID;
        sf::Vector2u tileID;
    };

    Selected();
    void draw(sf::RenderWindow &window);
};

Selected::Selected() {
    type = SelectedType::Nothing;
}