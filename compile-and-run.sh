g++ -std=c++17 -c main.cpp -O2 -I SFML-2.5.1/include/
g++ main.o -o evosim -L SFML-2.5.1/lib/ -lsfml-graphics -lsfml-window -lsfml-system
export LD_LIBRARY_PATH=SFML-2.5.1/lib && ./evosim