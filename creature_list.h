#ifndef NO_IO
#include <iostream>
#endif
#include <vector>
#include <utility>
#include <cmath>
#include <random>
#include <climits>

#include <SFML/Graphics.hpp>

#include "creature.h"
#include "histogram.h"

using namespace std;

class CreatureList {
    int seed;
    bool debug = true;
    vector<Creature> creatures;
    vector<unsigned int> unusedSpots;
    unsigned int creatureCount = 0;

    uniform_real_distribution<>
        randomXCoord{0.0, (double)WIDTH},
        randomYCoord{0.0, (double)HEIGHT};

    Creature nextCreature;


public:
    Histogram creatureCountHist {256, sf::Color(20, 255, 0)};
    unsigned long int nextID = 0ul;
    int minimumCreatureCount = MINIMUM_CREATURE_COUNT;

    default_random_engine randomEngine;

    CreatureList();
    void init(int seed);
    unsigned int addCreature(Creature &added);
    bool removeCreature(unsigned int slot);

    bool createClone(
        World &world,
        unsigned int slot,
        unsigned long int &nextID,
        default_random_engine &randomEngine
    );

    Creature const &getCreature(unsigned int slot);
    Creature const &getCreatureByID(unsigned long int creatureID);

    bool isActive(unsigned int slot);

    unsigned int count();
    unsigned int slotCount();

    unsigned int creatureAtPosition(
        sf::Vector2<double> point,
        double tolerance
    );    
    unsigned int creatureAtPosition(
        sf::Vector2<double> point,
        double tolerance,
        unsigned int start
    );
    unsigned int creatureAtPosition(sf::Vector2f point);

    void getValues(World &world);
    void applyUpdate(World &world, unsigned int selectedID);
    void draw(
        sf::RenderWindow &window,
        sf::Vector2f viewSize,
        sf::Vector2f viewLocation,
        bool debug
    );
};

CreatureList::CreatureList() {

}

void CreatureList::init(int seed) {
    this->seed = seed;
    randomEngine.seed(seed);
    for (int i = 0; i < minimumCreatureCount; i++) {
        nextCreature.randomInit(
            nextID,
            BrainType::subleqBrain,
            randomEngine, randomXCoord, randomYCoord
        );
        nextID++;

        addCreature(nextCreature);
    }
}

unsigned int CreatureList::addCreature(Creature &added) {
    //
    if (unusedSpots.size() >= 1) {
        creatures[unusedSpots.back()] = added;
        unusedSpots.pop_back();

        ++creatureCount;
        return unusedSpots.size() + 1;
    } else {
        creatures.push_back(added);
        ++creatureCount;
        return 0;
    }
}

bool CreatureList::removeCreature(unsigned int slot) {
    if (slot == UINT_MAX){
        return false;
    } else if (slot == creatures.size() - 1) {
        creatures.pop_back();
        --creatureCount;
        return true;
    } else {
        creatures[slot].deactivate();
        unusedSpots.push_back(slot);
        --creatureCount;
        return false;
    }
}

bool CreatureList::createClone(
    World &world,
    unsigned int slot,
    unsigned long int &nextID,
    default_random_engine &randomEngine
) {
    if (slot == UINT_MAX) {
        return false;
    } else {
        Creature addedCreature;
        Creature &originalCreature = creatures[slot];
        if (originalCreature.getActive() == false) {
            return false;
        }
        addedCreature.cloneInit(
            world,
            nextID,
            originalCreature.getLocation(),
            originalCreature.getBrain(),
            originalCreature.getID(),
            randomEngine
        );

#ifndef NO_IO
        if (debug) {
            cout 
              << "Creature #" << nextID 
              << " born at " 
              << addedCreature.getLocation().x << ", "
              << addedCreature.getLocation().y << "\n";
        }
#endif

        addCreature(addedCreature);

        nextID++;

        return true;
    }
}

Creature const &CreatureList::getCreature(unsigned int slot) {
    return creatures[slot];
}

bool CreatureList::isActive(unsigned int slot) {
    return creatures[slot].getActive();
}

unsigned int CreatureList::creatureAtPosition(
    sf::Vector2<double> point, double tolerance
) {
    for (int i = 0; i < creatures.size(); i++) {
        if (
            creatures[i].getActive()
         && vectorLength(creatures[i].getLocation() - point)
          < creatures[i].getSize() + tolerance
        ) {
            return i;
        }
    }
    return UINT_MAX;
}

unsigned int CreatureList::creatureAtPosition(
    sf::Vector2<double> point, double tolerance, unsigned int start
) {
    for (int i = start; i < creatures.size(); i++) {
        if (
            creatures[i].getActive()
         && vectorLength(creatures[i].getLocation() - point)
          < creatures[i].getSize() + tolerance
        ) {
            return i;
        }
    }
    return UINT_MAX;
}

unsigned int CreatureList::count() {
    return creatureCount;
}

unsigned int CreatureList::slotCount() {
    return creatures.size();
}

void CreatureList::getValues(World &world) {
    for (int i = count(); i < minimumCreatureCount; i++) {
        nextCreature.randomInit(
            nextID,
            BrainType::subleqBrain,
            randomEngine, randomXCoord, randomYCoord
        );
        nextID++;

        addCreature(nextCreature);
    }

    unsigned int seenCreatureID;
    double seenCreatureHue;

    for (int i = 0; i < creatures.size(); i++) {
        if (creatures[i].getActive()) {
            seenCreatureID = creatureAtPosition(
                creatures[i].getSensorLocation(), 0.0
            );
            if (seenCreatureID != UINT_MAX) {
                seenCreatureHue = creatures[seenCreatureID].getBodyHue();
            } else {
                seenCreatureHue = 0.0;
            }


            creatures[i].getValues(
                world, seenCreatureID != UINT_MAX, seenCreatureHue
            );
        }
    }
}

void CreatureList::applyUpdate(World &world, unsigned int selectedID) {
    UpdateStatus status;
    for (unsigned int i = 0; i < creatures.size(); i++) {
        if (creatures[i].getActive()) {
            status = creatures[i].applyUpdate(world, i == selectedID);

            if (!status.alive) {
#ifndef NO_IO
                if (debug) {
                    cout
                     << "Creature #" << to_string(creatures[i].getID())
                     << " died after " << to_string(creatures[i].getAge())
                     << " ticks" << "\n";
                }
#endif

                removeCreature(i);
            }

            if (status.givingBirth) {
                //cout << "9";
                createClone(world, i, nextID, randomEngine);
            }
            /*if (status.fighting > 0) {
                double totalEnergyShift = 0;
                unsigned int takenFrom;
                while ((takenFrom = creatureAtPosition()))
            }*/
        }
    }

    if (world.getAge() % HISTOGRAM_UPDATE_FREQ == 0) {
        creatureCountHist.addValue(count());
    }
}

void CreatureList::draw(
    sf::RenderWindow &window,
    sf::Vector2f viewSize, sf::Vector2f viewLocation,
    bool debug
) {
    this->debug = debug;
    int visibles = 0;
    for (Creature &creature : creatures) {
        if (creature.getActive()) {
            if (creature.isVisible(viewSize, viewLocation)) {
                creature.draw(window);
                visibles++;
            }
        }
    }
    //cout << visibles << "\n";
}