
void RenderState::handleMouseRelease(
    sf::Event &event,
    SimulationState &sim
) {

    if (event.mouseButton.button == sf::Mouse::Left) {
        pressLocation = {
            (float)event.mouseButton.x,
            (float)event.mouseButton.y
        };

        scrolling = false;
        totalScrolled = originalMousePos - pressLocation;

        if (
            hypot(totalScrolled.x, totalScrolled.y) < 10
            && pressLocation.x < worldPart * screenSize.x
        ) {
            tileLocation = screenToWorld(
                viewLocation,
                pressLocation / displayScale
            );

            if (sim.world.pointInBounds(tileLocation)) {
                unsigned int creatureSlot =
                sim.creatureList.creatureAtPosition(
                    (sf::Vector2<double>)tileLocation, 0.1
                );

                if (creatureSlot == UINT_MAX) {
                    selected.type = SelectedType::Tile;
                    selected.tileID = {
                        (unsigned int)tileLocation.x,
                        (unsigned int)tileLocation.y
                    };

                } else {
                    selected.type = SelectedType::Creature;
                    selected.creatureID = creatureSlot;
                }
            } else {
                selected.type = SelectedType::Nothing;                
            }

            /*viewCenter = worldView.getCenter()
                - totalScrolled
                / displayScale;
            viewLocation = viewCenter - (worldViewSize / 2.0f);
            worldView.setCenter(viewCenter);*/

        }
    }
}

void RenderState::handleKeyboardInput(
    sf::Event &event,
    SimulationState &sim
) {
    //cout << event.key.code << "\n";
    switch (event.key.code) {

        case sf::Keyboard::F:
            displayScale = fitWorldToScreen(
                sim.worldSize, screenSize, worldPart
            );

            worldViewSize = {
                ((float)screenSize.x)
                    / displayScale
                    * worldPart,
                ((float)screenSize.y)
                    / displayScale
            };

            worldView.setSize(worldViewSize);
            viewCenter = (sf::Vector2f)sim.worldSize / 2.0f;
            viewLocation = viewCenter - (worldViewSize / 2.0f);
            worldView.setCenter(viewCenter);
            //window.setView(worldView);
        break;

        case sf::Keyboard::L: //TODO: language swtching
        break;

        case sf::Keyboard::C:
            sim.creatureList.createClone(
                sim.world,
                getSelectedCreature(),
                sim.creatureList.nextID,
                sim.creatureList.randomEngine
            );
        break;

        case sf::Keyboard::K:
            sim.creatureList.removeCreature(
                getSelectedCreature()
            );
        break;

        case sf::Keyboard::O:
            drawMode = DrawMode::drawOriginal;
        break;
        case sf::Keyboard::I:
            drawMode = DrawMode::drawImproved;
        break;
        case sf::Keyboard::E:
            drawMode = DrawMode::drawElevation;
        break;
        case sf::Keyboard::H:
            drawMode = DrawMode::drawHumidity;
        break;
        case sf::Keyboard::T:
            drawMode = DrawMode::drawTemperature;
        break;
        case sf::Keyboard::Y:
            drawMode = DrawMode::drawEnergy;
        break;

        case sf::Keyboard::R:
            rendering = !rendering;
        break;

        case sf::Keyboard::Q:
            debug = !debug;
        break;

        case sf::Keyboard::Escape:
            selected.type = SelectedType::Nothing;
        break;

        case sf::Keyboard::A:
            if (selected.type == SelectedType::Creature) {
                do {
                    --selected.creatureID;
                } while (
                    selected.creatureID != 0 && selected.creatureID != UINT_MAX
                 && !sim.creatureList.isActive(selected.creatureID));
                if (selected.creatureID == UINT_MAX) {
                    selected.creatureID = 0;
                }
            }
        break;
        case sf::Keyboard::D:
            if (selected.type == SelectedType::Creature) {
                //cout << sim.creatureList.slotCount() << "\n";
                unsigned int oldSlot = selected.creatureID;
                do {
                    ++selected.creatureID;
                } while (
                    selected.creatureID < sim.creatureList.slotCount() - 1
                 && !sim.creatureList.isActive(selected.creatureID));
                if (selected.creatureID == sim.creatureList.slotCount()) {
                    selected.creatureID = oldSlot;
                }
            }
        break;

        case sf::Keyboard::B:
            screenshotTexture.create(screenSize.x, screenSize.y);
            screenshotTexture.update(window);
            screenshot = screenshotTexture.copyToImage();
            screenshot.saveToFile("test_screenshot.png");
        break;

        case sf::Keyboard::Add:
        case sf::Keyboard::Unknown: //handles the + key
            if (updatesPerFrame == 0) {
                updatesPerFrame = 1;
            } else if (updatesPerFrame < INT_MAX / 2) {
                updatesPerFrame *= 2;
            }
            //cout << updatesPerFrame << "\n";
        break;

        case sf::Keyboard::Subtract:
        case sf::Keyboard::Hyphen:
            if (updatesPerFrame == 1) {
                updatesPerFrame = 1;
            } else {
                updatesPerFrame /= 2;
            }
            //cout << updatesPerFrame << "\n";
        break;

        case sf::Keyboard::Enter:
            running = !running;
        break;

        case sf::Keyboard::Tab:
            running = false;
            sim.doFrame(getSelectedCreature());
        break;
    }
}

void RenderState::handleEvent(
    sf::Event &event,
    SimulationState &sim
) {
    switch (event.type) {
    case sf::Event::Closed:
        window.close();
        break;

    case sf::Event::Resized:
        screenSize.x = event.size.width;
        screenSize.y = event.size.height;

        infoPart = (screenSize.y * (7.0 / 9.0)) / screenSize.x;
        worldPart = 1 - infoPart;

        worldViewSize = {
            ((float)screenSize.x) / displayScale * worldPart,
            ((float)screenSize.y) / displayScale
        };

        infoViewSize = {
            ((float)screenSize.x) * infoPart,
            ((float)screenSize.y)
        };

        if (infoViewSize.y >= 702) {
            infoScale = 1;
            infoView.setCenter(infoViewSize / 2.0f);
        } else {
            infoScale = infoViewSize.y / 702;
            infoViewSize /= (float)infoScale;
        }


        worldView.setViewport(sf::FloatRect(0.0f, 0.0f, worldPart, 1.0f));
        infoView.setViewport(sf::FloatRect(worldPart, 0.0f, infoPart, 1.0f));

        worldView.setSize(worldViewSize);
        infoView.setSize(infoViewSize);

        //window.setView(worldView);
        viewCenter = worldView.getCenter();
        viewLocation = viewCenter - (worldViewSize / 2.0f);

#ifndef NO_IO
        if (debug) {
            cout << infoViewSize.y << "\n";
            cout << infoScale << "\n";
            cout 
             << infoView.getCenter().x << " "
             << infoView.getCenter().y << "\n";
        }
#endif
        break;

    case sf::Event::MouseWheelScrolled:
        scrolling = false;
        displayScale = max(
            fitWorldToScreen(
                sim.worldSize, screenSize, worldPart
            ),
            displayScale * pow(
                scrollSensitivity, event.mouseWheelScroll.delta
            )
        );

        worldViewSize = {
            ((float)screenSize.x) / displayScale * worldPart,
            ((float)screenSize.y) / displayScale
        };
        worldView.setSize(worldViewSize);
        //window.setView(worldView);
        viewCenter = worldView.getCenter();
        viewLocation = viewCenter - (worldViewSize / 2.0f);
        break;

    case sf::Event::MouseButtonPressed:
        if (event.mouseButton.button == sf::Mouse::Left) {
            scrolling = true;
            originalMousePos = {
                (float)event.mouseButton.x,
                (float)event.mouseButton.y
            };
            lastMousePos = originalMousePos;
        }
        break;

    case sf::Event::MouseMoved:
        if (scrolling == true) {
            newMousePos = {
                (float)event.mouseMove.x,
                (float)event.mouseMove.y
            };

            viewCenter = worldView.getCenter()
                + (lastMousePos - newMousePos)
                / displayScale;
            viewLocation = viewCenter - (worldViewSize / 2.0f);
            worldView.setCenter(viewCenter);

            lastMousePos = newMousePos;
            //window.setView(worldView);
        }
        break;

    case sf::Event::MouseButtonReleased:
        handleMouseRelease(event, sim);
        break;

    case sf::Event::KeyPressed:
        handleKeyboardInput(event, sim);
        break;
    }
}
