#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

using namespace std;

struct CreatureLocation {
    sf::Vector2f location;
    double size;
};

enum class TileType {
    water = 0,
    land = 1
};

struct CreatureData {
    unsigned long int creatureID;
    double energy, age, size;

    double direction, speed;

    double attackAbility, walkAbility, swimAbility,
        grazeAbility, heatResistance, coolResistance;

    sf::Vector2f sensorLocation;

    double bodyHue;
    sf::Color bodyColor;
};

struct CreatureInput {
    double bodyHue = 0, energy = 0, size = 0, speed = 0;
    double tileEnergy = 0, tileTemperature = 0, tileElevation = 0;
    TileType seenTileType = TileType::land;
    double seenTileEnergy = 0;
    bool canSeeCreature = 0;
    double creatureBodyHue = 0;
    double attackAbility = 0;
    double walkAbility = 0, swimAbility = 0, grazeAbility = 0;
    double heatResistance = 0, coolResistance = 0;
};

struct CreatureAction {
    double
        hue,
        acceleration, angleChange,
        grazing, fighting,
        increaseHeatResist, increaseCoolResist, increaseSize,
        givingBirth,
        sensorDistance, sensorAngle;
};

struct VisionData {
    double temperature, elevation, energy;
};

struct InputData {
    vector<CreatureLocation> creatureLocations;
    array<VisionData, 2> visionData;
};

class NetworkLayer {
    vector<double> axonWeights;
    int inputNeurons, outputNeurons;

public:
    NetworkLayer(vector<double> axonWeights);

    double activation(double input);

    vector<double> apply(vector<double> lastLayerOutput);

    vector<vector<double>> getAxonWeights();
};

class NeuralNetwork {
    //vector<NetworkLayer> layers;

public:
    NeuralNetwork(vector<NetworkLayer> layers);

    vector<double> computeOutput(vector<double> input);
};

struct SubleqMachine {
    static const unsigned int programSize = 256;
    static const unsigned int dataSize = 256;
    static const unsigned int executionCount = 256;

    array<uint8_t, programSize> program;
    array<uint8_t, dataSize> data;

    array<uint8_t, programSize> programHeatmap;
    array<uint8_t, dataSize> dataReadHeatmap;
    array<uint8_t, dataSize> dataWriteHeatmap;

    SubleqMachine();
    void init(
        array<uint8_t, programSize> program
    );
    void init(
        array<uint8_t, programSize> program,
        array<uint8_t, dataSize> data
    );
    void step(unsigned int instruction);
    void run(bool selected);

    //array<uint8_t, programSize> const &getProgram();
    //array<uint8_t, dataSize> const &getData();
    //array<uint8_t, programSize> const &getProgramHeatmap();
    //array<uint8_t, dataSize> const &getDataHeatmap();
};

void SubleqMachine::init(
    array<uint8_t, programSize> program
) {
    this->program = program;
    this->data.fill(1);
}

void SubleqMachine::run(bool selected) {
    uint8_t instructionPointer = 0, actedOn, subtracted, jumpTarget;

    if (selected) {
        //cout << "hi!" << "\n";
        dataReadHeatmap.fill(0);
        dataWriteHeatmap.fill(0);
        programHeatmap.fill(0);
    }

    // all required data values will have been already set

    for (int i = 0; i < executionCount; i++) {
        data[0xe4] = i;
        data[0xee] = 16;
        data[0xef] = 1;

        actedOn = program[instructionPointer++];
        subtracted = program[instructionPointer++];
        jumpTarget = program[instructionPointer++];

        if (selected) {
            ++programHeatmap[instructionPointer - 3];
            ++dataReadHeatmap[subtracted];
            ++dataWriteHeatmap[actedOn];
            //cout << int(subtracted) << " " << int(actedOn) << "\n";
        }

        /*cout
            << to_string(actedOn) << " "
            << to_string(subtracted) << " "
            << to_string(jumpTarget) << " "
            << i << " "
            << to_string(instructionPointer) << "\n";*/

        if (actedOn == 0xe3) {
            actedOn = data[0xe2];
        }
        if (subtracted == 0xe3) {
            subtracted = data[0xe2];
        }

        data[actedOn] -= data[subtracted];

        if (data[actedOn] < 0x7f) { // this technically makes it a subl machine
            instructionPointer = jumpTarget;
            /*cout << "jump" << "\n";*/
        }
    }

    //cout << "----------------" << "\n";
    /*for (uint8_t i : programHeatmap) {
        cout << to_string(i) << " ";
    }
    cout << "\n";/*

    for (uint8_t i : data) {
        cout << to_string(i) << " ";
    }
    cout << "\n";*/
}

enum class BrainType {
    subleqBrain,
    neuralBrain
};

struct Brain {
    BrainType type;

    union {
        SubleqMachine subleqBrain;
        NeuralNetwork neuralBrain;
    };

public:
    Brain();
    void initSubleq(
        array<uint8_t, SubleqMachine::programSize> program
    );

    CreatureAction run(
        double bodyHue, double energy, double size, double speed,
        double tileEnergy, double tileTemperature, double tileElevation,
        double seenTileSlope, double seenTileEnergy,
        bool canSeeCreature, double creatureBodyHue,
        double attackAbility, double walkAbility, double swimAbility,
        double grazeAbility, double heatResistance, double coolResistance
    );
    CreatureAction run(CreatureInput &in, bool selected);
};

Brain::Brain() {

}

void Brain::initSubleq(
    array<uint8_t, SubleqMachine::programSize> program
) {
    subleqBrain.init(program);
}

CreatureAction Brain::run(
   CreatureInput &in, bool selected
) {
    /*if (selected) {
        cout << "brain\n";
    }*/

    switch(type) {
    case BrainType::subleqBrain:
        // initialization

        subleqBrain.data[0xf0] = (uint8_t)(in.energy * 255);
        subleqBrain.data[0xf1] = (uint8_t)(in.size * 255);
        subleqBrain.data[0xf2] = (uint8_t)(in.speed * 255);

        subleqBrain.data[0xf3] = (uint8_t)(in.tileEnergy * 255);
        subleqBrain.data[0xf4] = (uint8_t)(in.tileTemperature * 255);
        subleqBrain.data[0xf5] = (uint8_t)(in.tileElevation * 255);

        subleqBrain.data[0xf6] = (uint8_t)((int)in.seenTileType * 128);
        subleqBrain.data[0xf7] = (uint8_t)(in.seenTileEnergy * 255);

        subleqBrain.data[0xf8] = (uint8_t)(in.canSeeCreature * 128);
        subleqBrain.data[0xf9] = (uint8_t)(in.creatureBodyHue * 255);

        subleqBrain.data[0xfa] = (uint8_t)(in.attackAbility * 255);
        subleqBrain.data[0xfb] = (uint8_t)(in.walkAbility * 255);
        subleqBrain.data[0xfc] = (uint8_t)(in.swimAbility * 255);
        subleqBrain.data[0xfd] = (uint8_t)(in.grazeAbility * 255);

        subleqBrain.data[0xfe] = (uint8_t)(in.heatResistance * 255);
        subleqBrain.data[0xff] = (uint8_t)(in.coolResistance * 255);

        // execution

        subleqBrain.run(selected);

        // interpretation
        /*        hue,
        acceleration, angleChange,
        grazing, fighting,
        increaseHeatResist, increaseCoolResist, increaseSize,
        givingBirth,
        sensorDistance, sensorAngle;*/

        return {
            (double)subleqBrain.data[0xe0] / 255,               //hue
            ((double)subleqBrain.data[0xe1] / 127.5) - 1,       //
            ((double)subleqBrain.data[0xe5] / 127.5) - 1,       //
            (double)subleqBrain.data[0xe6] / 255,
            ((double)subleqBrain.data[0xe7] / 127.5) - 1,
            ((double)subleqBrain.data[0xe8] / 127.5) - 1,
            ((double)subleqBrain.data[0xe9] / 127.5) - 1,
            ((double)subleqBrain.data[0xea] / 127.5) - 1,
            ((double)subleqBrain.data[0xeb] / 127.5) - 1,
            ((double)subleqBrain.data[0xec] / 255) + in.size,
            ((double)subleqBrain.data[0xed] / 255) * 2 * pi(),
        };

        break;
    }
}
